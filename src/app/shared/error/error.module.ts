import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './error.component';
import { StoreModule } from '@ngrx/store';
import * as fromError from './+state/error.reducer';
import { MatSnackBarModule } from '@angular/material';

@NgModule({
	declarations: [ErrorComponent],
	imports: [
		CommonModule,
		MatSnackBarModule,
		StoreModule.forFeature('error', fromError.reducer)
	],
	exports: [ErrorComponent]
})
export class ErrorModule { }
