import { Component, ChangeDetectionStrategy, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarRef } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { ErrorState, Error, getErrors } from './+state/error.reducer';
import { RemoveError } from './+state/error.actions';

@Component({
	selector: 'app-error',
	template: '',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorComponent implements OnInit, OnDestroy {
	unsubscribe$ = new Subject<void>();
	snackBarRef: MatSnackBarRef<any>;
	errors$: Error[];

	constructor(public snackBar: MatSnackBar, private store: Store<ErrorState>) { }

	ngOnInit() {
		this.store.pipe(select(getErrors, takeUntil(this.unsubscribe$))).subscribe(errors => {
			errors.forEach(item => {
				this.openSnackBar(item);
			});
		});
	}

	openSnackBar(error: Error) {
		this.snackBarRef = this.snackBar.open(error.message, error.actionLabel);

		this.snackBarRef.onAction().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.store.dispatch(error.action);
			this.store.dispatch(new RemoveError(error.id));
		});
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
	}
}
