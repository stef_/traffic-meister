import { Action, createFeatureSelector, createSelector } from '@ngrx/store';
import { ErrorActionTypes, ErrorActions } from './error.actions';

const uuid = () => {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		const r = (Math.random() * 16) | 0, // tslint:disable-line
			v = c === 'x' ? r : (r & 0x3) | 0x8;// tslint:disable-line
		return v.toString(16);
	});
};

export interface ErrorState {
	errors: Error[];
}

export interface Error {
	id: string;
	message: string;
	action: Action;
	actionLabel: string;
}

export const initialState: ErrorState = {
	errors: []
};

export function reducer(state = initialState, action: ErrorActions): ErrorState {
	switch (action.type) {
		case ErrorActionTypes.SetError:
			return {
				errors: [
					...state.errors,
					{
						id: uuid(),
						message: action.message,
						action: action.storeAction,
						actionLabel: action.label
					}
				]
			};
		case ErrorActionTypes.RemoveError:
			return {
				errors: state.errors.filter(e => e.id !== action.id)
			};
		default:
			return state;
	}
}

const getErrorState = createFeatureSelector<ErrorState>('error');
export const getErrors = createSelector(getErrorState, (state: ErrorState) => state.errors);


