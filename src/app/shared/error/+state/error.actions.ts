import { Action } from '@ngrx/store';

export enum ErrorActionTypes {
	SetError = '[Error] Set Error',
	RemoveError = '[Error] Remove Error'
}

export class SetError implements Action {
	readonly type = ErrorActionTypes.SetError;
	constructor(public message: string, public storeAction: Action, public label: string) { }
}

export class RemoveError implements Action {
	readonly type = ErrorActionTypes.RemoveError;
	constructor(public id: string) { }
}

export type ErrorActions = SetError | RemoveError;
