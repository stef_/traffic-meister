import {
	Component, OnInit, ChangeDetectionStrategy,
	Input, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { VehicleType, Filters } from '../+state/reducers/vehicles.interfaces';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
	selector: 'app-selectors',
	templateUrl: './selectors.component.html',
	styleUrls: ['./selectors.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectorsComponent implements OnInit, OnChanges, OnDestroy {

	unsubscribe$ = new Subject<void>();
	selectorsForm: FormGroup;

	@Input() brands: string[];
	@Input() types: VehicleType[];
	@Input() colors: string[];
	@Input() selections: Filters;


	@Output() filter = new EventEmitter<Filters>();

	constructor(private fb: FormBuilder) { }

	ngOnInit() {
		this.selectorsForm = this.fb.group({
			brandsControl: [],
			typesControl: [],
			colorsControl: []
		});

		this.selectorsForm.valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe(values => {
			this.filter.emit(values);
		});
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes['selections'] && this.selectorsForm) {
			this.selectorsForm.patchValue(this.selections, { emitEvent: false });
		}
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
	}
}
