import { Action } from '@ngrx/store';
import { Vehicle, Filters } from '../reducers/vehicles.interfaces';

export enum VehiclesActionTypes {
	LoadVehicles = '[Vehicles] Load Vehicles',
	LoadVehiclesSuccess = '[Vehicles] Load Vehicles Success',
	LoadVehiclesFail = '[Vehicles] Load Vehicles Fail',
	Filter = '[Vehicles] Filter',
	Undo = '[Vehicles] Undo',
	Redo = '[Vehicles] Redo'
}

export class LoadVehicles implements Action {
	readonly type = VehiclesActionTypes.LoadVehicles;
}

export class LoadVehiclesSuccess implements Action {
	readonly type = VehiclesActionTypes.LoadVehiclesSuccess;
	constructor(public data: Vehicle[]) { }
}

export class LoadVehiclesFail implements Action {
	readonly type = VehiclesActionTypes.LoadVehiclesFail;
	constructor(public error: string) { }
}

export class Filter implements Action {
	readonly type = VehiclesActionTypes.Filter;
	constructor(public filters: Filters) { }
}

export class Undo implements Action {
	readonly type = VehiclesActionTypes.Undo;
}

export class Redo implements Action {
	readonly type = VehiclesActionTypes.Redo;
}

export type VehiclesActions =
	LoadVehicles |
	LoadVehiclesSuccess |
	LoadVehiclesFail |
	Filter |
	Undo |
	Redo;
