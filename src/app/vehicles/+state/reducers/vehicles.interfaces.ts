export type VehicleType = 'car' | 'airplane' | 'train';

export interface Vehicle {
	id: number;
	type: VehicleType;
	brand: string;
	colors: string[];
	img: string;
}

export interface VehiclesStateUndoable {
	past: VehiclesState[];
	present: VehiclesState;
	future: VehiclesState[];
}

export interface VehiclesState {
	data: Array<Vehicle>;
	selections: Filters;
	loading: boolean;
}

export const initialState: VehiclesState = {
	data: [],
	selections: {
		brandsControl: null,
		colorsControl: null,
		typesControl: null
	},
	loading: false
};

export interface Filters {
	brandsControl: string | null;
	colorsControl: string | null;
	typesControl: string | null;
}

