import { ActionReducer, Action } from '@ngrx/store';
import { VehiclesActionTypes } from '../actions/vehicles.actions';
import { initialState } from './vehicles.interfaces';

const initialUndoableState = {
	past: [],
	present: initialState,
	future: []
};

export function undoRedo(reducer: ActionReducer<any>) {
	return (state = initialUndoableState, action: Action) => {
		const { past, present, future } = state;
		switch (action.type) {

			case VehiclesActionTypes.Undo:
				const previous = past[past.length - 1];
				const newPast = past.slice(0, past.length - 1);
				return {
					past: newPast,
					present: previous,
					future: [present, ...future]
				};
			case VehiclesActionTypes.Redo:
				const next = future[0];
				const newFuture = future.slice(1);
				return {
					past: [...past, present],
					present: next,
					future: newFuture
				};
			default:
				const newPresent = reducer(present, action);
				if (present === newPresent) {
					return state;
				}
				if (VehiclesActionTypes.Filter === action.type) {
					return {
						past: [...past, present],
						present: newPresent,
						future: []
					};
				}
				return {
					past: [...past],
					present: newPresent,
					future: []
				};
		}
	};
}
