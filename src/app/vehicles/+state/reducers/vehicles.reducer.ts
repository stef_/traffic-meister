import { VehiclesActions, VehiclesActionTypes } from '../actions/vehicles.actions';
import { initialState, VehiclesState } from './vehicles.interfaces';

export function reducer(state = initialState, action: VehiclesActions): VehiclesState {
	switch (action.type) {
		case VehiclesActionTypes.LoadVehicles:
			return { ...state, loading: true };
		case VehiclesActionTypes.LoadVehiclesSuccess:
			return {
				...state,
				data: action.data,
				loading: false
			};
		case VehiclesActionTypes.Filter:
			return {
				...state,
				selections: action.filters,
			};
		case VehiclesActionTypes.LoadVehiclesFail:
			return { ...state, loading: false };
		default:
			return state;
	}
}
