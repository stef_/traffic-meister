import { createFeatureSelector, createSelector } from '@ngrx/store';
import { VehiclesStateUndoable, Vehicle, Filters } from '../reducers/vehicles.interfaces';
import { pipe, mapDataToBrands, mapDataToTypes, mapDataToColors } from './index';

const getVehiclesState = createFeatureSelector<VehiclesStateUndoable>('vehicles');
const getData = createSelector(getVehiclesState, (vehicles: VehiclesStateUndoable) => vehicles.present.data);
const getLoading = createSelector(getVehiclesState, (vehicles: VehiclesStateUndoable) => vehicles.present.loading);
const getSelections = createSelector(getVehiclesState, (vehicles: VehiclesStateUndoable) => vehicles.present.selections);
const getPastStatesNum = createSelector(getVehiclesState, (vehicles: VehiclesStateUndoable) => vehicles.past.length);
const getFutureStatesNum = createSelector(getVehiclesState, (vehicles: VehiclesStateUndoable) => vehicles.future.length);

const getTypes = createSelector(getData, getSelections,
	(data: Vehicle[], selections: Filters) => {
		const filteredByBrand = (_data) => _data.filter(item => item.brand === selections.brandsControl || !selections.brandsControl);
		const filteredByColor = (_data) => _data.filter(item => item.colors.indexOf(selections.colorsControl) > -1 || !selections.colorsControl);
		const filteredData = pipe(
			filteredByBrand,
			filteredByColor
		)(data);

		return mapDataToTypes(filteredData);
	});

const getBrands = createSelector(getData, getSelections,
	(data: Vehicle[], selections: Filters) => {
		const filteredByType = (_data) => _data.filter(item => item.type === selections.typesControl || !selections.typesControl);
		const filteredByColor = (_data) => _data.filter(item => item.colors.indexOf(selections.colorsControl) > -1 || !selections.colorsControl);
		const filteredData = pipe(
			filteredByType,
			filteredByColor
		)(data);

		return mapDataToBrands(filteredData);
	});

const getColors = createSelector(getData, getSelections,
	(data: Vehicle[], selections: Filters) => {
		const filteredByBrand = (_data) => _data.filter(item => item.brand === selections.brandsControl || !selections.brandsControl);
		const filteredByType = (_data) => _data.filter(item => item.type === selections.typesControl || !selections.typesControl);
		const filteredData = pipe(
			filteredByBrand,
			filteredByType
		)(data);

		return mapDataToColors(filteredData);
	});

const getFilteredData = createSelector(getData, getSelections,
	(data: Vehicle[], selections: Filters) => {
		const filteredByBrand = (_data) => _data.filter(item => item.brand === selections.brandsControl || !selections.brandsControl);
		const filteredByType = (_data) => _data.filter(item => item.type === selections.typesControl || !selections.typesControl);
		const filteredByColor = (_data) => _data.filter(item => item.colors.indexOf(selections.colorsControl) > -1 || !selections.colorsControl);
		const filteredData = pipe(
			filteredByBrand,
			filteredByType,
			filteredByColor
		)(data);

		return filteredData;
	});

export const vehiclesQuery = {
	getData,
	getTypes,
	getBrands,
	getColors,
	getLoading,
	getSelections,
	getPastStatesNum,
	getFutureStatesNum,
	getFilteredData
};

