import { Vehicle, VehicleType } from '../reducers/vehicles.interfaces';

export const mapDataToBrands = (data: Array<Vehicle>): Array<string> => {
	return data.reduce((brands, item) => {
		if (brands.indexOf(item.brand) > -1) {
			return brands;
		}
		return [...brands, item.brand];
	}, []);
};

export const mapDataToTypes = (data: Array<Vehicle>): Array<VehicleType> => {
	return data.reduce((types, item) => {
		if (types.indexOf(item.type) > -1) {
			return types;
		}
		return [...types, item.type];
	}, []);
};

export const mapDataToColors = (data: Array<Vehicle>): Array<string> => {
	return data.reduce((colors, item) => {
		const newColors = item.colors.filter(color => colors.indexOf(color) === -1);
		return [...colors, ...newColors];
	}, []);
};

export const pipe = (...fns: Function[]) => (x: Vehicle[]): Vehicle[] => fns.reduce((v, f) => f(v), x);
