import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { LoadVehicles, LoadVehiclesSuccess, LoadVehiclesFail, Filter, Undo, Redo } from '../actions/vehicles.actions';
import { vehiclesQuery } from './vehicles.selectors';
import { VehiclesState, Vehicle, Filters } from '../reducers/vehicles.interfaces';

@Injectable()
export class VehiclesFacade {
	data$ = this.store.pipe(select(vehiclesQuery.getFilteredData));
	brands$ = this.store.pipe(select(vehiclesQuery.getBrands));
	types$ = this.store.pipe(select(vehiclesQuery.getTypes));
	colors$ = this.store.pipe(select(vehiclesQuery.getColors));
	loading$ = this.store.pipe(select(vehiclesQuery.getLoading));
	selections$ = this.store.pipe(select(vehiclesQuery.getSelections));
	futureStatesNum$ = this.store.pipe(select(vehiclesQuery.getFutureStatesNum));
	pastStatesNum$ = this.store.pipe(select(vehiclesQuery.getPastStatesNum));

	constructor(private store: Store<VehiclesState>) { }

	loadVehicles() {
		this.store.dispatch(new LoadVehicles());
	}

	loadVehiclesSuccess(data: Array<Vehicle>) {
		this.store.dispatch(new LoadVehiclesSuccess(data));
	}

	loadVehiclesFail(error: string) {
		this.store.dispatch(new LoadVehiclesFail(error));
	}

	filter(filters: Filters) {
		this.store.dispatch(new Filter(filters));
	}

	undo() {
		this.store.dispatch(new Undo());
	}

	redo() {
		this.store.dispatch(new Redo());
	}
}
