import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { VehiclesEffects } from './vehicles.effects';

describe('VehiclesEffects', () => {
	const actions$: Observable<any> = undefined;
	let effects: VehiclesEffects;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				VehiclesEffects,
				provideMockActions(() => actions$)
			]
		});

		effects = TestBed.get(VehiclesEffects);
	});

	it('should be created', () => {
		expect(effects).toBeTruthy();
	});
});
