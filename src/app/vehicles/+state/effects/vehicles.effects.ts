import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { VehiclesActionTypes, LoadVehiclesSuccess, LoadVehicles } from '../actions/vehicles.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import * as trafficMeister from '../../../../service';
import { Observable, of } from 'rxjs';
import { Vehicle } from '../reducers/vehicles.interfaces';
import { SetError } from 'src/app/shared/error/+state/error.actions';


@Injectable()
export class VehiclesEffects {

	@Effect()
	loadVehicles$ = this.actions$.pipe(
		ofType(VehiclesActionTypes.LoadVehicles),
		switchMap(_ => Observable.create(observer => trafficMeister.fetchData((error, data) => {
			if (!error) {
				observer.next(data);
			} else {
				observer.error(error);
			}
		})).pipe(
			map((result: Vehicle[]) => new LoadVehiclesSuccess(result)),
			catchError((error: string) => of(new SetError(error, new LoadVehicles(), 'Reload')))
		))
	);

	constructor(private actions$: Actions) { }
}
