import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionsInfoComponent } from './selections-info.component';

describe('SelectionsInfoComponent', () => {
	let component: SelectionsInfoComponent;
	let fixture: ComponentFixture<SelectionsInfoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SelectionsInfoComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SelectionsInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
