import { Component, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Filters } from '../+state/reducers/vehicles.interfaces';

@Component({
	selector: 'app-selections-info',
	templateUrl: './selections-info.component.html',
	styleUrls: ['./selections-info.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectionsInfoComponent implements OnChanges {

	selectedOptions: string;

	@Input() pastStatesNum: number;
	@Input() futureStatesNum: number;
	@Input() selections: Filters;

	@Output() undo = new EventEmitter<void>();
	@Output() redo = new EventEmitter<void>();

	ngOnChanges(changes: SimpleChanges) {
		if (changes['selections']) { this.selectedOptions = this.mapSelectedOptionsToString(this.selections); }
	}

	private mapSelectedOptionsToString = (values: Filters): string => {
		return [values['brandsControl'], values['typesControl'], values['colorsControl']]
			.filter(i => !!i)
			.join(', ');
	}

}
