import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { VehiclesFacade } from './+state/utils/vehicles.facade';
import { Vehicle, VehicleType, Filters } from './+state/reducers/vehicles.interfaces';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-vehicles',
	templateUrl: './vehicles.component.html',
	styleUrls: ['./vehicles.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class VehiclesComponent implements OnInit {
	vehicles$: Observable<Vehicle[]>;
	brands$: Observable<string[]>;
	types$: Observable<VehicleType[]>;
	colors$: Observable<string[]>;
	loading$: Observable<boolean>;
	selections$: Observable<Filters>;
	pastStatesNum$: Observable<number>;
	futureStatesNum$: Observable<number>;

	constructor(private vehiclesFacade: VehiclesFacade) { }

	ngOnInit() {
		this.vehiclesFacade.loadVehicles();
		this.vehicles$ = this.vehiclesFacade.data$;
		this.brands$ = this.vehiclesFacade.brands$;
		this.types$ = this.vehiclesFacade.types$;
		this.colors$ = this.vehiclesFacade.colors$;
		this.loading$ = this.vehiclesFacade.loading$;
		this.selections$ = this.vehiclesFacade.selections$;
		this.pastStatesNum$ = this.vehiclesFacade.pastStatesNum$;
		this.futureStatesNum$ = this.vehiclesFacade.futureStatesNum$;
	}

	onFilter(event: Filters) {
		this.vehiclesFacade.filter(event);
	}

	onUndo() {
		this.vehiclesFacade.undo();
	}

	onRedo() {
		this.vehiclesFacade.redo();
	}
}
