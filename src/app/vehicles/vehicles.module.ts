import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatSelectModule, MatButtonModule, MatSnackBarModule, MatBadgeModule } from '@angular/material';
import { VehiclesComponent } from './vehicles.component';
import { VehiclesRoutingModule } from './vehicles-routing.module';
import { StoreModule } from '@ngrx/store';
import * as fromVehicles from './+state/reducers/vehicles.reducer';
import { EffectsModule } from '@ngrx/effects';
import { VehiclesEffects } from './+state/effects/vehicles.effects';
import { VehiclesFacade } from './+state/utils/vehicles.facade';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { SelectorsComponent } from './selectors/selectors.component';
import { ReactiveFormsModule } from '@angular/forms';
import { undoRedo } from './+state/reducers/undo-redo.reducer';
import { LoaderModule } from '../shared/loader/loader.module';
import { SelectionsInfoComponent } from './selections-info/selections-info.component';

@NgModule({
	declarations: [VehiclesComponent, VehicleListComponent, SelectorsComponent, SelectionsInfoComponent],
	imports: [
		ReactiveFormsModule,
		CommonModule,
		VehiclesRoutingModule,
		MatCardModule,
		MatButtonModule,
		MatSelectModule,
		MatSnackBarModule,
		MatBadgeModule,
		StoreModule.forFeature('vehicles', fromVehicles.reducer, { metaReducers: [undoRedo] }),
		EffectsModule.forFeature([VehiclesEffects]),
		LoaderModule
	],
	providers: [VehiclesFacade]
})
export class VehiclesModule { }
