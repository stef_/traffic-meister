import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { Vehicle } from '../+state/reducers/vehicles.interfaces';

@Component({
	selector: 'app-vehicle-list',
	templateUrl: './vehicle-list.component.html',
	styleUrls: ['./vehicle-list.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class VehicleListComponent {
	@Input() vehicles: Vehicle[];
}
