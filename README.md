# TrafficMeister

  
# [DEMO](https://traffic-meister-7be94.firebaseapp.com/)

  
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.


## Development server


Clone the project, run `npm install` and then `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

  
## Build


Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Structure of the project

#### A picture is worth a thousand words

![Diagram](diagram.png)

## Design patterns in the application

For this app, I followed the Redux design pattern. I implemented it here through the ngrx/platform library which is an implementation of the Redux design pattern in the Angular ecosystem. According to this pattern, when we want to make a change to the UI, we dispatch an action, then this action is handled from some pure functions which called reducers and then the UI observes the changes from the reducers.

  

In the application you can find also the smart-dumb components design pattern:

- The smart components observe the changes in the state of our app through specific selectors and provides the dumb components with specific '@Inputs()'.

- The dumb components depend only on their inputs to render the appropriate information. This dependance of the dumb components only on their inputs and the fact that these inputs change in an immutable way (because of reducers which are pure functions that receive a state and expose a clone of this state), is significant, because we can perform changeDetectionStrategy.OnPush in our components. This strategy has performance benefits for our app.


## Decisions about specific modules and components

- The vehicles module is a lazy loaded module, so it is loaded when we navigate to this route. In our case, because it's the only route that we have in the application and because we redirect to this path when we navigate to the initial route, lazy-loading does not offer something in the performance but when we design an application, we should have in our minds that will be added more modules and routes in the future.

The vehicles.module consists of four components. The vehicles.component is a smart component which "knows" how to retrieve the data which are stored in the state and provides these data to the other three dumb components.

- The error module is able to track multiple errors in the application from different components and routes. You have only to dispatch an action with a payload in order to see the error in the page . Again, it was built with the notion that should be generic(it would be much more easier to implement a simple component which would be shown an error but I don't want to implement things which are not reusable and are against the DRY logic). If you want to test the error component, please go to 'src/app/service/index.js' file and add (Math.random() < 0.5) to test error messages with a theoretical 50% chance of error.

- In order to make clear the benefits and the power of the redux design pattern in our app, I implemented a feature which wasn't asked which enables the user to undo and redo his selections.